import 'package:flutter/material.dart'; //引入material ui樣式包

import 'package:fitness/pages/exercise.dart';
import 'package:fitness/pages/routine.dart';
import 'package:fitness/pages/home.dart';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:advanced_icon/advanced_icon.dart';

// color
// brand color: Color.fromRGBO(251, 77, 65, 1)
// medium red: Color.fromRGBO(255, 131, 113, 1)
// light red: Color.fromRGBO(255, 168, 149, 1)
// white:  Color.fromRGBO(250, 250, 250, 1)
// black:  Color.fromRGBO(66, 66, 66, 1)
// grey2:  Color.fromRGBO(117, 117, 117, 1)
// grey3:  Color.fromRGBO(189, 189, 189, 1)
// grey3:  Color.fromRGBO(238, 238, 238, 1)
//  gradient: LinearGradient(
//     colors: <Color>[
//       Color.fromRGBO(251, 77, 65, 1),
//       Color.fromRGBO(255, 168, 149, 1)
//     ],
//     begin: Alignment.topLeft,
//     end: Alignment.bottomRight,
// ),
void main() => runApp(
      MyApp(),
    );

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _appState();
}

class _appState extends State<MyApp> {
  GlobalKey<CurvedNavigationBarState> navigationKey = GlobalKey();
  int _page = 1;
  final screens = [
    const Routine(),
    const Home(),
    const Excercise(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Fitness Tracker",
      home: Container(
        // color: Colors.white,
        color: Colors.transparent,
        child: SafeArea(
            top: false, //ios底部橫杠在導航底下
            bottom: false,
            child: ClipRect(
              child: Scaffold(
                backgroundColor: Color.fromRGBO(238, 238, 238, 1),
                bottomNavigationBar: CurvedNavigationBar(
                  key: navigationKey,
                  index: 1,
                  height: 75.0,
                  items: const <Widget>[
                    AdvancedIcon(
                      // icon: Icons.self_improvement,
                      icon: Icons.sports_gymnastics,
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color.fromRGBO(251, 77, 65, 1),
                          Color.fromRGBO(255, 168, 149, 1)
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    AdvancedIcon(
                      icon: Icons.home,
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color.fromRGBO(251, 77, 65, 1),
                          Color.fromRGBO(255, 168, 149, 1)
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    AdvancedIcon(
                      icon: Icons.signal_cellular_alt,
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color.fromRGBO(251, 77, 65, 1),
                          Color.fromRGBO(255, 168, 149, 1)
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                  ],
                  color: Colors.white, //nav bar bg color
                  // buttonBackgroundColor: const Color.fromRGBO(250, 650, 250, 1), //selected btn bg color
                  backgroundColor: Colors.transparent, //bg of the
                  animationCurve: Curves.easeInOut,
                  animationDuration: const Duration(milliseconds: 280),
                  onTap: (index) {
                    setState(() {
                      _page = index;
                    });
                  },
                  letIndexChange: (index) => true,
                ),
                body: screens[_page],
              ),
            )),
      ),
    );
  }
}























// void main() => runApp(
//       MyApp(
//           //動態列表：傳參
//           items: List<String>.generate(
//               60, (i) => "三連 ${i + 1} 次") //Item1,Item2...Item1000
//           //String類型的List
//           //生成器: generate(length, generator命名方法)
//           ),
//     ); //入口main()

//有動態數據的頁面
// class MyApp extends StatefulWidget {
//   //繼承StatelessWidget:靜態組件，狀態不變
//   //動態列表：接受參數
//   final List<String> items;
//   const MyApp({Key? key, required this.items}) : super(key: key);
//   @override
//   State<MyApp> createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   var name = '小雨在烦恼什么';
//   List<Quote> data = [
//     Quote(author: '小雨', text: '三連了嘛？！'),
//     Quote(author: '小雨', text: '三連了嘛？！'),
//     Quote(author: '小雨', text: '三連了嘛？！'),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "title",
//       home: Scaffold(
//         //腳手架
//         appBar: AppBar(
//           //app頂部
//           title: Text(name), //文本widget
//           backgroundColor: const Color.fromRGBO(79, 131, 127, 1), //背景色，顏色不能用hex
//           elevation: 0, //去掉陰影
//         ),
//         floatingActionButton: FloatingActionButton(
//           onPressed: () {
//             setState(() {
//               name = '哈哈哈';
//             });
//           },
//           backgroundColor: const Color.fromRGBO(255, 255, 255, 0.5),
//           elevation: 0,
//           child: const Icon(Icons.edit),
//         ),
//         body: Container(
//           width: 500,
//           decoration: const BoxDecoration(
//             gradient: LinearGradient(
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//                 colors: [
//                   Color.fromRGBO(79, 131, 127, 1),
//                   Colors.cyan,
//                 ]), //漸變色。color: Colors.purple,//純色背景
//           ),
//           child: Column(
//             children: data
//                 .map((item) => quoteCard(
//                     data: item,
//                     delete: () {
//                       setState(() => data.remove(item));
//                     }))
//                 .toList(),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class quoteCard extends StatelessWidget {
//   final Quote data;
//   final VoidCallback delete; //不能是Function,會報錯
//   quoteCard({required this.data, required this.delete});

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       child: Padding(
//         padding: const EdgeInsets.all(18.0),
//         child: Column(
//           children: <Widget>[
//             Text(
//               data.text,
//               style: const TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 18,
//                   color: Colors.grey),
//             ),
//             const SizedBox(height: 6.0),
//             Text(
//               data.author,
//               style: const TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 14,
//                   color: Colors.grey),
//             ),
//             const SizedBox(height: 6.0),
//             ElevatedButton(
//               style: ButtonStyle(
//                 backgroundColor: MaterialStateProperty.all(Colors.amber),
//               ),
//               onPressed: delete,
//               child: const Text('三連啦！'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

//靜態頁面
// class MyApp extends StatelessWidget {
//   //繼承StatelessWidget:靜態組件，狀態不變
//   //動態列表：接受參數
//   final List<String> items; //聲明變量
//   const MyApp({Key? key, required this.items})
//       : super(key: key); //看不懂這句需要去看dart語法

//   @override //重寫
//   Widget build(BuildContext context) {
//     //返回一個build組件
//     return MaterialApp(
//       title: "title",
//       home: Scaffold(
//         //腳手架
//         appBar: AppBar(
//           //app頂部
//           title: const Text('App Title'), //文本widget
//           backgroundColor: const Color.fromRGBO(79, 131, 127, 1), //背景色，顏色不能用hex
//           elevation: 0, //去掉陰影
//         ),
//         body: Container(
//           //container組件類似div
//           alignment: Alignment.topCenter,
//           //width: 500,
//           // height: 500,
//           padding: const EdgeInsets.fromLTRB(
//               10, 5, 0, 5), // (左，上，右，下) EdgeInsets.all(10)統一設為10,margin也一樣寫法
//           decoration: const BoxDecoration(
//             gradient: LinearGradient(
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//                 colors: [
//                   Color.fromRGBO(79, 131, 127, 1),
//                   Colors.cyan,
//                 ]), //漸變色。color: Colors.purple,//純色背景
//           ),
//           //拆分組件（兩個例子：1.靜態的list；2. 動態的list)
//           // child: MyList(), //引入MyList
//           child: DynamicList(items: items), //動態列表：引入DynamicList()
//           //文字组件：
//           // child: const Text(
//           //   'body.center.text Hello World',
//           //   style: TextStyle(
//           //     fontSize: 30,
//           //   ),
//           // ),
//         ),
//       ),
//     );
//   }
// }

//拆分組件1：（把ListView單獨拆出來）
// class MyList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       //橫向列表例子
//       scrollDirection: Axis.horizontal,
//       children: <Widget>[
//         Container(
//           width: 500,
//           height: 500,
//           child: const Image(
//             image: NetworkImage(
//                 'https://i.pinimg.com/564x/e4/48/86/e4488625e1e75cda2b3c5b33a4a0afa9.jpg'),
//             // image: AssetImage('images/xxx.png'), //使用本地圖片的寫法
//           ),
//         ),
//         Container(
//           width: 500,
//           height: 500,
//           child: const Image(
//             image: NetworkImage(
//                 "https://i.pinimg.com/564x/e4/48/86/e4488625e1e75cda2b3c5b33a4a0afa9.jpg"),
//           ),
//         ),

//         //豎向列表例子
//         // ListTile(
//         //   leading: Icon(Icons.add),
//         //   title: Text('title1'),
//         // ),
//         // ListTile(
//         //   leading: Icon(Icons.add),
//         //   title: Text('title2'),
//         // ),
//         // ListTile(
//         //   leading: Icon(Icons.add),
//         //   title: Text('title3'),
//         // ),
//       ],
//     );
//   }
// }

// //拆分組件2：動態列表例子,接受剛開始傳入的列表items,渲染一個列表
// class DynamicList extends StatelessWidget {
//   final List<String> items; //聲明變量
//   const DynamicList({Key? key, required this.items}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ListView.builder(
//         itemCount: items.length, //列表長度
//         itemBuilder: (context, index) {
//           return ListTile(
//             title: Text(
//               items[index],
//               style: const TextStyle(
//                 color: Colors.white,
//                 fontWeight: FontWeight.bold,
//               ),
//             ),
//           );
//         });
//   }
// }


// vs code快捷鍵
// R: 熱更新
// o: 切換安卓/ios
// p: 顯示網絡
// q: 退出運行
// statelessWidget to statefulWidget: cmd + .(mac), ctrl + . (win)