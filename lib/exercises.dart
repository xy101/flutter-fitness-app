class Exercises {
  String id;
  String name;
  int reps;
  int duration;
  String type;
  String guide;
  List equipment;
  List muscles;
  Exercises(
      {required this.id,
      required this.name,
      required this.reps,
      required this.duration,
      required this.type,
      required this.guide,
      required this.muscles,
      required this.equipment});
}

// type:
// arm
// chest
// shoulder
// back
// abdomen
// legs
// cardio

//equipment
//Dumbbells
//Barbell
//Elastic Band
//Exercise Ball

List<Exercises> exercises = [
  Exercises(
    id: '1',
    name: 'Push Ups',
    reps: 30,
    duration: 0,
    type: 'chest',
    guide:
        'keeping a prone position, with the hands palms down under the shoulders, the balls of the feet on the ground, and the back straight, pushes the body up and lets it down by an alternate straightening and bending of the arms.',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '2',
    name: 'Crunches',
    reps: 30,
    duration: 0,
    type: 'abdomen',
    guide:
        'Lie down on the mat, keep your knees bent, your back and feet flat, and your hands supporting your head. Lift your shoulders, squeeze your abdominal muscles and hold for 1 to 2 seconds.  Slowly return to the starting position and repeat until set is complete.',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '3',
    name: 'Pull Ups',
    reps: 30,
    duration: 0,
    type: 'back',
    guide:
        'Grip an overhead bar and lift your body until your chin is above that bar',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '4',
    name: 'Squats',
    reps: 30,
    duration: 0,
    type: 'legs',
    guide: 'Lower hips from a standing position and then stand back up',
    muscles: [],
    equipment: ['Dumbbells'],
  ),
  Exercises(
    id: '5',
    name: 'Lunges',
    reps: 30,
    duration: 0,
    type: 'legs',
    guide:
        'Stand tall with feet hip-width apart. Engage your core. Take a big step forward with right leg. Start to shift your weight forward so heel hits the floor first.  Lower your body until right thigh is parallel to the floor and right shin is vertical. It’s OK if knee shifts forward a little as long as it doesn’t go past right toe. If mobility allows, lightly tap left knee to the floor while keeping weight in right heel. Press into right heel to drive back up to starting position.',
    muscles: [],
    equipment: ['Dumbbells'],
  ),
  Exercises(
    id: '6',
    name: 'Burpees',
    reps: 30,
    duration: 0,
    type: 'cardio',
    guide:
        'Stand with your feet shoulder-width apart and your arms by your sides. Lower into a squat position and place your hands on the floor. Kick or step your legs back into a plank position. Jump or step your legs forward to return to a squat position. Return to the standing position.',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '7',
    name: 'Planks',
    reps: 30,
    duration: 30,
    type: 'abdomen',
    guide:
        'holding the trunk part of your body in a straight line off the ground',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '8',
    name: 'Deadlifts',
    reps: 30,
    duration: 0,
    type: 'legs',
    guide:
        'hips hinge backward to lower down and pick up a weighted barbell or kettlebell from the floor. Your back is flat throughout the movement.',
    muscles: [],
    equipment: ['Dumbbells'],
  ),
  Exercises(
    id: '9',
    name: 'Jumping Jacks',
    reps: 30,
    duration: 0,
    type: 'cardio',
    guide:
        'Stand upright with your legs together, arms at your sides. Bend your knees slightly, and jump into the air.you jump, spread your legs to be about shoulder-width apart. Stretch your arms out and over your head. Jump back to starting position',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '10',
    name: 'Bridge',
    reps: 30,
    duration: 0,
    type: 'abdomen',
    guide:
        'Lie on your back with your knees bent. Tighten your abdominal muscles. Raise your hips off the floor until your hips are aligned with your knees and shoulders. Hold for three deep breaths. Return to the starting position and repeat.',
    muscles: [],
    equipment: ['Dumbbells', 'Elastic Band'],
  ),
  Exercises(
    id: '11',
    name: 'Mountain Climber',
    reps: 30,
    duration: 0,
    type: 'abdomen',
    guide:
        'Performed from a plank position, alternate by bringing one knee to your chest, then back out again.',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '12',
    name: 'Chest Press',
    reps: 30,
    duration: 0,
    type: 'chest',
    guide:
        'Lie down on an exercise mat with knees bent and a light dumbbell in each hand. Extend elbows to a 90-degree position with the back of your arms resting on the floor',
    muscles: [],
    equipment: ['Dumbbells'],
  ),
  Exercises(
    id: '13',
    name: '劉畊宏',
    reps: 1,
    duration: 0,
    type: 'cardio',
    guide: '',
    muscles: [],
    equipment: [],
  ),
  Exercises(
    id: '14',
    name: 'Shoulder Press',
    reps: 10,
    duration: 0,
    type: 'shoulders',
    guide:
        'Stand with feet shoulder-width apart. Hold a pair of dumbbells at chin height, with arms bent close against your body and palms facing in.Press the weights up overhead, so your biceps frame your face. Hold for a second or so, then lower back to the starting position for a count of three.',
    muscles: [],
    equipment: ['Dumbbells'],
  ),
];
