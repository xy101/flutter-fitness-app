import 'package:flutter/material.dart';

class Excercise extends StatefulWidget {
  const Excercise({Key? key}) : super(key: key);

  @override
  State<Excercise> createState() => _ExcerciseState();
}

class _ExcerciseState extends State<Excercise> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(238, 238, 238, 1),
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(251, 77, 65, 1),
                Color.fromRGBO(255, 168, 149, 1)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: ElevatedButton.icon(
            onPressed: () {},
            icon: Icon(Icons.thumb_up),
            label: Text(
              '一键三连',
              style: TextStyle(color: Colors.white),
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.transparent),
              elevation: MaterialStateProperty.all(0),
            ),
          ),
        ),
      ),
    );
  }
}
