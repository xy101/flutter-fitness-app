import 'package:flutter/material.dart';
import 'package:fitness/routines.dart';
import 'package:advanced_icon/advanced_icon.dart';

class Routine extends StatefulWidget {
  const Routine({Key? key}) : super(key: key);

  @override
  State<Routine> createState() => _RoutineState();
}

Map routineTypeColor = {
  'upper': [Color.fromRGBO(252, 229, 221, 1), Icons.sports_handball],
  'lower': [Color.fromRGBO(231, 230, 228, 1), Icons.sports_gymnastics],
  'core': [Color.fromRGBO(243, 236, 228, 1), Icons.self_improvement],
  'full': [Color.fromRGBO(247, 187, 159, 0.6), Icons.sports_martial_arts],
};
List<Widget> todayRoutines() {
  return routines
      .map(
        (item) => Container(
          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
          decoration: BoxDecoration(
            color: routineTypeColor[item.type][0],
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(189, 189, 189, 0.3),
                spreadRadius: 3,
                blurRadius: 8,
                offset: Offset(0, 2), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
            padding: EdgeInsets.all(25),
            child: Flex(
              direction: Axis.horizontal,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                  child: Icon(
                    routineTypeColor[item.type][1],
                    color: Color.fromRGBO(117, 117, 117, 1),
                  ),
                ),
                Flex(
                  direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        item.name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          color: Color.fromRGBO(66, 66, 66, 1),
                        ),
                      ),
                    ),
                    Chip(
                      avatar: const CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: AdvancedIcon(
                          icon: Icons.timer,
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color.fromRGBO(251, 77, 65, 1),
                              Color.fromRGBO(255, 168, 149, 1)
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                      ),
                      // padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                      label: Text(
                        item.duration,
                        style: const TextStyle(
                          color: Color.fromRGBO(66, 66, 66, 0.8),
                          fontSize: 12,
                        ),
                      ),
                      backgroundColor: Colors.transparent,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      )
      .toList();
}

class _RoutineState extends State<Routine> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(238, 238, 238, 1),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(30, 88, 30, 0),
        child: Flex(
          direction: Axis.vertical,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Text(
                'My Workouts',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Color.fromRGBO(66, 66, 66, 1)),
              ),
            ),
            ListView(
              shrinkWrap: true,
              children: todayRoutines(),
            ),
          ],
        ),
      ),
    );
  }
}
