import 'package:flutter/material.dart';
import 'package:fitness/date.dart';
import 'package:fitness/routines.dart';
import 'package:advanced_icon/advanced_icon.dart';

// gradient: LinearGradient(
//     colors: <Color>[
//       Color.fromRGBO(251, 77, 65, 1),
//       Color.fromRGBO(255, 168, 149, 1)
//     ],
//     begin: Alignment.topLeft,
//     end: Alignment.bottomRight,
// ),

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var name = 'Tina';
  var month = 'July';
  var hours = '45';
  var calories = '618';
  String todayIndex = '3';

  Map routineTypeColor = {
    'upper': [Color.fromRGBO(252, 229, 221, 1), Icons.sports_handball],
    'lower': [Color.fromRGBO(231, 230, 228, 1), Icons.sports_gymnastics],
    'core': [Color.fromRGBO(243, 236, 228, 1), Icons.self_improvement],
    'full': [Color.fromRGBO(247, 187, 159, 0.6), Icons.sports_martial_arts],
  };

  List<Color> colors = [
    const Color.fromRGBO(255, 168, 149, 1),
    const Color.fromRGBO(251, 77, 65, 1),
  ];

  List<Widget> dateNav() {
    return dates
        .map(
          (f) => ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: f.date == todayIndex
                      ? colors
                      : [Colors.transparent, Colors.transparent],
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      child: Text(
                        f.day,
                        style: TextStyle(
                          fontSize: 13,
                          color: f.date == todayIndex
                              ? Colors.white
                              : Color.fromRGBO(66, 66, 66, 0.6),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        f.date,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: f.date == todayIndex
                              ? Colors.white
                              : Color.fromRGBO(66, 66, 66, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
        .toList();
  }

  List<Widget> todayRoutines() {
    return routines
        .map(
          (item) => Container(
            color: routineTypeColor[item.type][0],
            child: Padding(
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
              child: Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Icon(
                    routineTypeColor[item.type][1],
                    color: Color.fromRGBO(117, 117, 117, 1),
                  ),
                  Text(
                    item.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(66, 66, 66, 1),
                    ),
                  ),
                  Chip(
                    // padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                    label: Text(
                      item.duration,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(66, 66, 66, 1),
                      ),
                    ),
                    backgroundColor: Colors.transparent,
                  ),
                ],
              ),
            ),
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(238, 238, 238, 1),
        body: Flex(
          direction: Axis.vertical,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 88, 30, 0),
              child: Column(
                children: <Widget>[
                  Text(
                    'Hello, $name',
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Color.fromRGBO(66, 66, 66, 1)),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 30),
                    child: Flex(
                      direction: Axis.horizontal,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: dateNav(),
                    ),
                  ),
                  Flex(
                    direction: Axis.horizontal,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Chip(
                        padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
                        avatar: const CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: AdvancedIcon(
                            icon: Icons.timer,
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color.fromRGBO(251, 77, 65, 1),
                                Color.fromRGBO(255, 168, 149, 1)
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                        ),
                        label: Text(
                          '${hours} Min',
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(66, 66, 66, 1),
                          ),
                        ),
                        // labelPadding: EdgeInsets.fromLTRB(3, 3, 35, 3),
                        backgroundColor: Colors.transparent,
                      ),
                      Chip(
                        padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
                        avatar: const CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: AdvancedIcon(
                            icon: Icons.energy_savings_leaf,
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color.fromRGBO(251, 77, 65, 1),
                                Color.fromRGBO(255, 168, 149, 1)
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                        ),
                        label: Text(
                          '${calories} Cal',
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(66, 66, 66, 1),
                          ),
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Flex(
              direction: Axis.vertical,
              children: [
                MaterialButton(
                  onPressed: () {},
                  child: Icon(Icons.add),
                  minWidth: double.infinity,
                  color: Colors.white,
                  height: 60,
                  textColor: Color.fromRGBO(251, 77, 65, 1),
                  elevation: 0,
                ),
                ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(top: 0),
                  children: todayRoutines(),
                ),
                SizedBox(height: 25),
              ],
            ),
          ],
        ));
  }
}
