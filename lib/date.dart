class Dates {
  String day;
  String date;

  Dates({required this.day, required this.date});
}

List<Dates> dates = [
  Dates(day: 'Mon', date: '1'),
  Dates(day: 'Tue', date: '2'),
  Dates(day: 'Wed', date: '3'),
  Dates(day: 'Thu', date: '4'),
  Dates(day: 'Fri', date: '5'),
];
