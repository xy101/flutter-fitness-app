class Routines {
  String id;
  String name;
  String type;
  String duration;
  int sets;
  List exercise;
  Routines(
      {required this.id,
      required this.name,
      required this.type,
      required this.duration,
      required this.exercise,
      required this.sets});
}

List<Routines> routines = [
  Routines(
    id: '1',
    name: 'My Upper Body Workout',
    type: 'upper',
    sets: 4,
    duration: '20 min',
    exercise: ['Push Ups', 'Crunches', 'Pull Ups', 'Chest Press'],
  ),
  Routines(
    id: '2',
    name: 'My Leg Workout',
    type: 'lower',
    sets: 3,
    duration: '30 min',
    exercise: ['Squats', 'Lunges', 'Deadlifts'],
  ),
  Routines(
    id: '3',
    name: 'Ger Abs!',
    type: 'core',
    sets: 2,
    duration: '25 min',
    exercise: ['Crunches', 'Mountain Climber', 'Planks', 'Bridge', 'Burpees'],
  ),
  Routines(
    id: '4',
    name: '劉畊宏直播',
    type: 'full',
    sets: 1,
    duration: '60 min',
    exercise: ['劉畊宏'],
  ),
];

//  type:
//  1 upper  Color.fromRGBO(252, 229, 221, 1)
//  2 lower  Color.fromRGBO(231, 230, 228, 1)
// 3  core  Color.fromRGBO(243, 236, 228, 1)
// 4  full  Color.fromRGBO(247, 187, 159, 1)
